cmake_minimum_required(VERSION 3.12)
project(chap1 C)

set(CMAKE_C_STANDARD 11)

include_directories(.)

add_executable(chap1
        main.c
        prog1.c
        prog1.h
        slp.c
        slp.h
        util.c
        util.h
        func.c
        func.h)
