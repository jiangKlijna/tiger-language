
#include <stdio.h>
#include <string.h>
#include "util.h"
#include "slp.h"
#include "func.h"

typedef struct table *Table_;
struct table {
    string id;
    int value;
    Table_ tail;
};

Table_ Table(string id, int value, Table_ tail) {
    Table_ t = checked_malloc(sizeof *t);
    t->id = id;
    t->value = value;
    t->tail = tail;
    return t;
}

struct IntAndTable {
    int i;
    Table_ t;
};

Table_ interp_stm(A_stm s, Table_ t);

struct IntAndTable interp_exp(A_exp e, Table_ t);

// Generate new Table_ from str and val
Table_ update(Table_ t, string str, int val);

// Look up int value by Table_
int lookup(Table_ t, string key);

void interp(A_stm s) {
    interp_stm(s, NULL);
}

Table_ interp_stm(A_stm s, Table_ t) {
    if (s->kind == A_compoundStm) {
        Table_ t1 = interp_stm(s->u.compound.stm1, t);
        return interp_stm(s->u.compound.stm2, t1);
    } else if (s->kind == A_assignStm) {
        struct IntAndTable it = interp_exp(s->u.assign.exp, t);
        return update(it.t, s->u.assign.id, it.i);
    } else if (s->kind == A_printStm) {
        Table_ nt = t;
        A_expList list = s->u.print.exps;
        while (list != NULL) {
            if (list->kind == A_lastExpList) {
                struct IntAndTable it = interp_exp(list->u.last, nt);
                printf("%d ", it.i);
                break;
            } else {
                struct IntAndTable it = interp_exp(list->u.pair.head, nt);
                printf("%d ", it.i);
            }
            list = list->u.pair.tail;
        }
        putchar('\n');
        return nt;
    }
    return t;
}

struct IntAndTable interp_exp(A_exp exp, Table_ t) {
    if (exp->kind == A_eseqExp) {
        Table_ t1 = interp_stm(exp->u.eseq.stm, t);
        return interp_exp(exp->u.eseq.exp, t1);
    } else if (exp->kind == A_idExp) {
        struct IntAndTable it = {lookup(t, exp->u.id), t};
        return it;
    } else if (exp->kind == A_numExp) {
        struct IntAndTable it = {exp->u.num, t};
        return it;
    } else if (exp->kind == A_opExp) {
        struct IntAndTable it1 = interp_exp(exp->u.op.left, t);
        struct IntAndTable it2 = interp_exp(exp->u.op.right, it1.t);
        if (exp->u.op.oper == A_plus) {
            it2.i = it1.i + it2.i;
        } else if (exp->u.op.oper == A_minus) {
            it2.i = it1.i - it2.i;
        } else if (exp->u.op.oper == A_times) {
            it2.i = it1.i * it2.i;
        } else if (exp->u.op.oper == A_div) {
            it2.i = it1.i / it2.i;
        }
        return it2;
    }
}

Table_ update(Table_ t, string str, int val) {
    return Table(str, val, t);
}

int lookup(Table_ t, string key) {
    Table_ iter = t;
    while (iter != NULL) {
        if (strcmp(key, iter->id) == 0) {
            return iter->value;
        }
        iter = iter->tail;
    }
}

int maxargs(A_stm s) {
    if (s->kind == A_compoundStm) {
        int a = maxargs(s->u.compound.stm1);
        int b = maxargs(s->u.compound.stm2);
        return a > b ? a : b;
    } else if (s->kind == A_assignStm) {
        int max = 0;
        A_exp exp = s->u.assign.exp;
        while (exp != NULL) {
            if (exp->kind == A_eseqExp) {
                int v = maxargs(exp->u.eseq.stm);
                if (v > max) max = v;
                exp = exp->u.eseq.exp;
            } else break;
        }
        return max;
    } else if (s->kind == A_printStm) {
        int max = 0;
        int i = 0;
        A_expList list = s->u.print.exps;
        while (++i) {
            if (list->kind == A_lastExpList) {
                break;
            }
            if (list->u.pair.head->kind == A_eseqExp) {
                int v = maxargs(list->u.pair.head->u.eseq.stm);
                if (v > max) max = v;
            }
            list = list->u.pair.tail;
        }
        return i > max ? i : max;
    }
    return 0;
}

void foreach(A_stm s) {
    if (s->kind == A_compoundStm) {
        foreach(s->u.compound.stm1);
        foreach(s->u.compound.stm2);
        printf("A_compoundStm\n");
    } else if (s->kind == A_assignStm) {
        A_exp exp = s->u.assign.exp;
        while (exp != NULL) {
            if (exp->kind == A_eseqExp) {
                foreach(exp->u.eseq.stm);
                exp = exp->u.eseq.exp;
            } else break;
        }
        printf("A_assignStm:%s\n", s->u.assign.id);
    } else if (s->kind == A_printStm) {
        int i = 0;
        A_expList list = s->u.print.exps;
        while (++i) {
            if (list->kind == A_lastExpList) {
                break;
            }
            if (list->u.pair.head->kind == A_eseqExp) {
                foreach(list->u.pair.head->u.eseq.stm);
            }
            list = list->u.pair.tail;
        }
        printf("A_printStm:%d\n", i);
    }
}
