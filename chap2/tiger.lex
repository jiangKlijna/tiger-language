%{
#include <stdio.h>
#include <string.h>
#include "util.h"
#include "tokens.h"
#include "errormsg.h"

#define RETURN_TOK(TOK) adjust(); return TOK;

int charPos=1;
int commentDepth = 0;

int yywrap(void)
{
 charPos=1;
 return 1;
}


void adjust(void)
{
 EM_tokPos=charPos;
 charPos+=yyleng;
}

string trimQuotes(string str)
{
    str++;
    str[strlen(str)-1] = '\0';
    return str;
}

%}

%x COMMENT STR
%%
[ \t]+    {adjust(); continue;}
\n	      {adjust(); EM_newline(); continue;}
"/*"      {adjust(); BEGIN(COMMENT);commentDepth++;}
<COMMENT>{
"/*"	  {adjust(); commentDepth++;}
"*/"	  {adjust(); if(--commentDepth == 0)BEGIN(INITIAL);}
[^\n]	  {adjust();}
(\n|\r\n) {adjust();EM_newline();}
}
","	      {RETURN_TOK(COMMA);}
":"       {RETURN_TOK(COLON);}
";"       {RETURN_TOK(SEMICOLON);}
"["       {RETURN_TOK(LPAREN);}
"]"       {RETURN_TOK(RPAREN);}
"("       {RETURN_TOK(LBRACK);}
")"       {RETURN_TOK(RBRACK);}
"{"       {RETURN_TOK(LBRACE);}
"}"       {RETURN_TOK(RBRACE);}
"."       {RETURN_TOK(DOT);}
"+"       {RETURN_TOK(PLUS);}
"-"       {RETURN_TOK(MINUS);}
"*"       {RETURN_TOK(TIMES);}
"/"       {RETURN_TOK(DIVIDE);}
"="       {RETURN_TOK(EQ);}
"!="      {RETURN_TOK(NEQ);}
"<"       {RETURN_TOK(LT);}
"<="      {RETURN_TOK(LE);}
">"       {RETURN_TOK(GT);}
">="      {RETURN_TOK(GE);}
"&"       {RETURN_TOK(AND);}
"|"       {RETURN_TOK(OR);}
":="      {RETURN_TOK(ASSIGN);}
array     {RETURN_TOK(ARRAY);}
if        {RETURN_TOK(IF);}
then      {RETURN_TOK(THEN);}
else      {RETURN_TOK(ELSE);}
while     {RETURN_TOK(WHILE);}
for  	  {RETURN_TOK(FOR);}
to  	  {RETURN_TOK(TO);}
do  	  {RETURN_TOK(DO);}
let  	  {RETURN_TOK(LET);}
in        {RETURN_TOK(IN);}
end       {RETURN_TOK(END);}
of        {RETURN_TOK(OF);}
break     {RETURN_TOK(BREAK);}
nil       {RETURN_TOK(NIL);}
function  {RETURN_TOK(FUNCTION);}
var       {RETURN_TOK(VAR);}
type      {RETURN_TOK(TYPE);}
[0-9]+	  {adjust(); yylval.ival=atoi(yytext); return INT;}
[a-zA-Z][a-zA-Z0-9]* {adjust(); yylval.sval=yytext; return ID;}
\"(\\.|[^"\\])*\" {adjust(); yylval.sval=trimQuotes(yytext); return STRING;}
.	      {adjust(); EM_error(EM_tokPos,"illegal token");}


